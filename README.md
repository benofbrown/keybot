Simple discord bot to manage stream keys.

Configuration
-------------

Create a file called `.env` in the directory you'll run the script from. Here's an example one:

    DISCORD_TOKEN=<app_token>
    DISCORD_GUILD=<guild id>
    DISCORD_CHANNEL=<channel id>
    KEYADMIN_ROLE=<role id>
    KEYSERVER_URL=http://localhost:8000/
    ICS_URL=<url to ics file for the calendar to query>
    NOTIFY_MESSAGE_ID=<id of the message to handle reactions to>
    NOTIFY_ROLE_ID=<id of the role to add or remove when the message above is reacted to>
    NBE_USER=<username to use the Nightbot Extensions API to get and set info about each streamer>
    NBE_PASSWORD=<password to use the Nightbot Extensions API to get and set info about each streamer>
    NBE_URL=<URL for the Nightbot Extensions API to get and set info about each streamer>
    WEBHOOK_CHANNEL=<id of the discord channel to listen for the webhook messages>
    NOTIFICATION_CHANNEL=<id of the discord channel to send messages to>

Replace the bits in `<` and `>` with the values for your discord instance. `DISCORD_GUILD`, `DISCORD_CHANNEL`, `NOTIFY_MESSAGE_ID`, `NOTIFY_ROLE_ID`, and `KEYADMIN_ROLE` all need to be set to the numeric ID numbers for the items they represent.

Nightbot Extensions
-------------------

This is an API primarily used by Nightbot to show information about active streaming keys in YouTube chat. You can get it here: https://github.com/nickstokes/nightbot-extensions/

This is needed for the `info` DM commands.

Access
------

All users in the guild (aka server) set in the config can query the bot via DM, though some commands are restricted to people with the role set as the `KEYADMIN_ROLE` in the config.

DM Commands
-----------

The following commands are currently available:

- `help`: print out a list of the commands
- `active`: Show the current active streamer. People with the keyadmin role see a more detailed response
- `kick`: Kick the active streamer and lock them out for a specified time. Requires the keyadmin role. The time you set is pretty flexible, e.g. `5m` will lock it out for 5 minutes, `30s` for 30 seconds, `1m23s` for one minute, 23 seconds etc. If a time is not specified the key is locked for one minute.
- `forcekick`: Kick the active streamer and lock them out for a specified time. Requires the keyadmin role. Unlike `kick` this won't wait for the next time data is sent using that key, it will set the key state to be idle straight away. This shouldn't ever be needed but better to have it in case `kick` isn't working as expected. 
- `ping`: Replies with "pong". Just for checking the bot is responding to queries.
- `next`: Show what's next in the calender found at ICS_URL.
- `show`: Show any keys assigned to the person sending the DM.
- `info`: Manage info related to the currently active stream key. There are two subcommands:
    - `info get`: Fetch information for your keys.
    - `info set`: Set information for your keys. If you have more than one key, you will need to specifiy the key. e.g. `info set aaaa-bbbb-cccc-dddd The coolest streamer ever!`

Mention Commands
----------------

The bot also listens to the channel set in the config as `DISCORD_CHANNEL`, for messages in the format of `@keybot <command> @user`. You can specify multiple users if you like. The bot will only respond of the people sending the message have the keyadmin role. Currently the following commands are supported:

- `key for`: send keys to the mentioned users via DM, generating new keys if necessary
- `low priority key for`: send keys with a lower priority to the mentioned users via DM, generating them if necessary

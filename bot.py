#!/usr/bin/env python3

import aiohttp
import asyncio
import discord
import icalendar
import os
import pytz
import recurring_ical_events

from datetime import datetime, timedelta
from dotenv import load_dotenv
from typing import Union
from urllib.parse import urljoin


DEFAULT_PRIORITY = 12

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = int(os.getenv('DISCORD_GUILD'))
CHANNEL = int(os.getenv('DISCORD_CHANNEL'))
WEBHOOK_CHANNEL = int(os.getenv('WEBHOOK_CHANNEL'))
NOTIFICATION_CHANNEL = int(os.getenv('NOTIFICATION_CHANNEL'))
KEYADMIN_ROLE = int(os.getenv('KEYADMIN_ROLE'))
KEYSERVER_URL = os.getenv('KEYSERVER_URL')
ICS_URL = os.getenv('ICS_URL')
NOTIFY_MESSAGE_ID = int(os.getenv('NOTIFY_MESSAGE_ID'))
NOTIFY_ROLE_ID = int(os.getenv('NOTIFY_ROLE_ID'))
NBE_USER = os.getenv('NBE_USER')
NBE_PASSWORD = os.getenv('NBE_PASSWORD')
NBE_URL = os.getenv('NBE_URL')


class StreamKey(object):
    def __init__(self, data):
        self.id = data.get('id')
        self.nick = data.get('nick')
        self.priority = data.get('priority')
        self.state = data.get('state')
        if data.get('discord_id'):
            self.discord_id = int(data.get('discord_id'))
        else:
            self.discord_id = None

    def __str__(self):
        return f'ID: {self.id} Nick: "{self.nick}" Priority: {self.priority} State: {self.state}'

    def __repr__(self):
        return self.__str__()

    @property
    def relative_priority(self):
        if self.priority == DEFAULT_PRIORITY:
            return "Normal"
        elif self.priority > DEFAULT_PRIORITY:
            return "High"
        else:
            return "Low"

class HTTPClient(object):
    def __init__(self):
        self.auth = None

    async def do_request(self, method, url, json=None):
        try:
            async with aiohttp.request(method, url, auth=self.auth, json=json) as r:
                if r.status in [200, 202]:
                    if r.content_type == 'application/json':
                        js = await r.json()
                        return js

                    text = await r.text()
                    return text

                elif r.status == 404:
                    return False
        except aiohttp.client_exceptions.ClientConnectorError:
            return None

        return None


class APIClient(HTTPClient):
    def __init__(self, base_url):
        self.base_url = base_url
        super().__init__()

    async def do_request(self, method, url, json=None):
        return await super().do_request(method, urljoin(self.base_url, url), json=json)

    async def get_key_by_id(self, key_id):
        key = await self.do_request('GET', f'/api/key/{key_id}')
        if key:
            return StreamKey(key)
        return key

    async def get_key_by_discord_id(self, discord_id):
        keys = await self.do_request('GET', f'/api/key/?discord_id={discord_id}')
        if keys:
            return [StreamKey(k) for k in keys]
        return keys

    async def get_key_by_nick(self, nick):
        keys = await self.do_request('GET', f'/api/key/?nick={nick}')
        if keys:
            return [StreamKey(k) for k in keys]
        return keys

    async def new_key(self, nick, discord_id, priority):
        payload = {'nick': nick, 'discord_id': discord_id, 'priority': priority}
        key = await self.do_request('POST', '/api/key/', json=payload)
        return StreamKey(key)

    async def get_active(self):
        key = await self.do_request('GET', '/api/key/active')
        if key:
            return StreamKey(key)
        return key

    async def kick(self, key_id, cooldown, force=False):
        payload = {'cooldown': cooldown, 'force': force}
        return await self.do_request('PUT', f'/api/key/{key_id}/disconnect', json=payload)


class CalendarFetcher(HTTPClient):
    def __init__(self, url):
        self.url = url
        super().__init__()

    async def get_next_event(self):
        calendar_text = await self.do_request('GET', self.url)
        try:
            calendar = icalendar.Calendar.from_ical(calendar_text)
        except TypeError:
            return None
        except ValueError:
            return None

        now = datetime.now(pytz.UTC)
        events = recurring_ical_events.of(calendar).between(now, now + timedelta(days=7))
        if not events:
            return None

        events.sort(key=lambda x: x['DTSTART'].dt)
        return events[0]


class NightbotExtensions(HTTPClient):
    def __init__(self, base_url, user, password):
        self.base_url = base_url
        self.auth = aiohttp.BasicAuth(user, password)

    async def get_current_info(self):
        return await self.do_request('GET', urljoin(self.base_url, '/api/nbe/get-streamer-info'))

    async def get_info_by_key(self, key_id):
        return await self.do_request('GET', urljoin(self.base_url, f'/api/nbe/streamer-info/{key_id}'))

    async def set_info_for_key(self, key_id, info_text):
        payload = {'stream_key_id': key_id, 'info_text': info_text}
        return await self.do_request('PUT', urljoin(self.base_url, '/api/nbe/streamer-info'), json=payload)


class StreamCard:
    description = "Join us on Twitch for more active chat!"
    thumbnail = "https://cdn.discordapp.com/attachments/758341897453437049/758342103779901440/EMS.png"
    standard_fields = [
        {"name": "Watch on", "value": "[**[Twitch]**](http://twitch.tv/earthmodularsociety) [**[YouTube]**](http://youtube.com/c/EarthModularSociety/live)"},
        {"name": "Don't forget to check out the site!", "value": "https://earthmodularsociety.com/", "inline": False},
    ]
    footer_text = "Brought to you by EMS ;)"

    def __init__(self, key: StreamKey, member: Union[discord.Member, str], mention: str):
        self.key = key
        self.member = member
        self.mention = mention

    def render(self):
        embed = discord.Embed(description=self.description, color=0x1e2936)
        embed.set_thumbnail(url=self.thumbnail)
        if isinstance(self.member, discord.Member):
            avatar = str(str(self.member.avatar_url_as(format=None, static_format="jpg")).split('?')[0])
            embed.set_author(name=self.member.display_name, icon_url=avatar)
            streamer = self.member.display_name
        else:
            embed.set_author(name=self.member)
            streamer = self.member

        for field in self.standard_fields:
            embed.add_field(name=field["name"], value=field["value"], inline=field.get("inline", True))
        embed.set_footer(text=self.footer_text, icon_url=self.thumbnail)

        if int(self.key.priority) > 10:
            content = f"Come watch {streamer} LIVE {self.mention}"
        else:
            content = f"Hold MuZaK provided by: {streamer}"

        return embed, content


class MyClient(discord.Client):
    def __init__(self, intents=None, session=None):
        self.guild = None
        self.API = None
        self.NBE = None
        self.notify_role = None
        self.ignore = {}
        self.notification_channel = None
        super().__init__(intents=intents)

        self.dm_commands = {
            'active': self.dm_active,
            'kick': self.dm_kick,
            'forcekick': self.dm_forcekick,
            'help': self.dm_help,
            'ping': self.dm_ping,
            'show': self.dm_show,
            'next': self.dm_next,
            'info': self.dm_info,
        }

        self.hidden_commands = {
            'ignore': self.dm_ignore,
            'unignore': self.dm_unignore,
        }

        self.calendar = CalendarFetcher(ICS_URL)

    def key_admin_check(self, user_id):
        member = self.guild.get_member(user_id)
        if not member:
            return None

        has_role = False
        for role in member.roles:
            if role.id == KEYADMIN_ROLE:
                has_role = True
                break

        return has_role

    async def process_reaction(self, payload):
        if payload.message_id != NOTIFY_MESSAGE_ID:
            return

        member = self.guild.get_member(payload.user_id)
        if not member:
            return

        if payload.event_type == 'REACTION_ADD':
            await member.add_roles(self.notify_role)
        elif payload.event_type == 'REACTION_REMOVE':
            await member.remove_roles(self.notify_role)
        else:
            return

    async def on_raw_reaction_add(self, payload):
        await self.process_reaction(payload)
        return

    async def on_raw_reaction_remove(self, payload):
        await self.process_reaction(payload)
        return

    async def on_message(self, message):
        if message.webhook_id and message.channel.id == WEBHOOK_CHANNEL:
            await self.handle_webhook(message)
            return

        if message.author == self.user:
            return

        if isinstance(message.author, discord.user.User):
            await self.handle_dm(message)
            return

        if message.author.id in self.ignore:
            return

        if message.channel.id != CHANNEL:
            return

        if not self.key_admin_check(message.author.id):
            return

        other_mentions = []
        mentions_me = False
        for mention in message.mentions:
            if mention.id == self.user.id:
                mentions_me = True
            else:
                other_mentions.append(mention)

        if not mentions_me:
            return

        if 'low priority key for' in message.content.lower():
            print(f'Low priority key request from {message.author.name}')
            all_rc = []
            for mention in other_mentions:
                rc = await self.key_generate_for_mention(mention, priority=10)
                all_rc.append(rc)

            if all(all_rc):
                await message.channel.send("All done!")
            else:
                await message.channel.send("Sorry, that didn't work out, try again later")
            return

        if 'key for' in message.content.lower():
            print(f'Key request from {message.author.name}')
            all_rc = []
            for mention in other_mentions:
                rc = await self.key_generate_for_mention(mention)
                all_rc.append(rc)

            if all(all_rc):
                await message.channel.send("On it!")
            else:
                await message.channel.send("Sorry, that didn't work out, try again later")
            return

        await message.channel.send("Sorry, I don't know what you mean")

    async def handle_webhook(self, message):
        if message.content != "blorp":
            return

        active_key = await self.API.get_active()
        if not active_key:
            return

        if active_key.discord_id:
            try:
                member = await self.guild.fetch_member(active_key.discord_id)
            except discord.errors.NotFound:
                return
        else:
            if active_key.nick:
                member = active_key.nick
            else:
                return


        card = StreamCard(active_key, member, self.notify_role.mention)
        embed, content = card.render()

        await self.notification_channel.send(embed=embed, content=content)

    async def handle_dm(self, message):
        has_admin = self.key_admin_check(message.author.id)
        if has_admin is None:
            return

        words = message.content.lower().strip().split()
        if has_admin:
            command = self.hidden_commands.get(words[0])
            if command:
                return await command(message)

        if message.author.id in self.ignore:
            return

        command = self.dm_commands.get(words[0])
        if not command:
            await message.author.send("Sorry, I don't know what you mean")
            return

        await command(message, has_admin)


    async def dm_ignore(self, message):
        self.ignore[message.author.id] = True
        await message.author.send("I will ignore you from now on")

    async def dm_unignore(self, message):
        del self.ignore[message.author.id]
        await message.author.send("Welcome back!")

    async def dm_show(self, message, has_admin):
        member, key_found = await self.key_find(message.author)
        if not member:
            return

        if not key_found:
            msg = f"Sorry, I don't have any keys for you. Ask an admin for a key in <#{CHANNEL}>"
            await message.author.send(msg)

    async def dm_ping(self, message, has_admin):
        await message.author.send("pong")

    async def dm_help(self, message, has_admin):
        member = self.guild.get_member(message.author.id)
        if not member:
            return

        out = "I know the following commands:\n\n"
        for command in sorted(self.dm_commands.keys()):
            out += f'- {command}\n'

        await message.author.send(out)

    async def dm_active(self, message, has_admin):
        key = await self.API.get_active()
        if key is None:
            await message.author.send("Sorry, something went wrong. Try again later")
            return

        if not key:
            await message.author.send("There are no active streams right now")
            return

        if has_admin:
            await message.author.send(f"```{key}```")
            return

        name = f'<@{key.discord_id}>' if key.discord_id else key.nick
        await message.author.send(f'{name} is streaming right now')

    async def dm_kick(self, message, has_admin, force=False):
        if not has_admin:
            await message.author.send("Sorry, you're not allowed to kick")
            return

        key = await self.API.get_active()
        if key is None:
            await message.author.send("Sorry, something went wrong. Try again later")
            return

        if not key:
            await message.author.send("There's no-one active to kick")
            return

        words = message.content.strip().split()
        cooldown = "1m"
        if len(words) > 1:
            cooldown = words[1]

        kick = await self.API.kick(key.id, cooldown, force=force)
        if kick is None:
            await message.author.send(f"Something went wrong, try again later")
            return

        await message.author.send(f"{'Force ' if force else ''}Kicked {key.nick} for {cooldown}")
        return

    async def dm_forcekick(self, message, has_admin):
        await self.dm_kick(message, has_admin, force=True)
        return

    async def dm_next(self, message, has_admin):
        next_event = await self.calendar.get_next_event()
        if not next_event:
            await message.author.send("Sorry, I couldn't find the next event")
            return

        from_now = next_event['DTSTART'].dt - datetime.now(pytz.UTC)
        hours = int(from_now.seconds / 3600) + (from_now.days * 24)
        minutes = int((from_now.seconds - (hours * 3600)) / 60)
        relative = f"in {hours} hour{'' if hours == 1 else 's'}"
        if minutes:
            relative += f" and {minutes} minute{'' if minutes == 1 else 's'}"
        await message.author.send(f"The next event is **{next_event['SUMMARY']}** at {next_event['DTSTART'].dt.strftime('%H:%M %Z %d %b %Y')} _({relative})_")

    async def dm_info(self, message, has_admin):
        words = message.content.lower().strip().split()
        words += [''] * (3 - len(words))

        if words[1] == 'get':
            return await self.get_info(words, message, has_admin)
        elif words[1] == 'set':
            return await self.set_info(words, message, has_admin)

        info = await self.NBE.get_current_info()
        if info:
            await message.author.send(info)
        else:
            await message.author.send("I couldn't get the information on the current stream, sorry")

    async def get_info(self, words, message, has_admin):
        sorry = "Sorry, I couldn't find any information for that key"
        if not words[2]:
            keys = await self.API.get_key_by_discord_id(message.author.id)
            if not keys:
                return await message.author.send("you don't have any keys")

            out = ''
            for key in keys:
                key_info = await self.NBE.get_info_by_key(key.id)
                if not key_info:
                    key_info = 'None set'

                out += f'`{key.id}`: {key_info}\n'
            return await message.author.send(out)

        key_id = words[2]
        key = await self.API.get_key_by_id(key_id)
        if key:
            if not has_admin:
                if message.author.id != key.discord_id:
                    return await message.author.send(sorry)

            info = await self.NBE.get_info_by_key(key.id)
            if info:
                await message.author.send(info)
            else:
                await message.author.send(sorry)

            return
        else:
            if has_admin:
                await message.author.send("I couldn't find that key")
            else:
                await message.author.send(sorry)

    async def set_info(self, words, message, has_admin):
        usage = '`usage: info set <optional key_id> <information to set>`'
        if not words[2]:
            return await message.author.send('`usage: info set <optional key_id> <information to set>`')

        key = await self.API.get_key_by_id(words[2])
        if key:
            if not has_admin:
                if key.discord_id != message.author.id:
                    return await message.author.send("Sorry, that didn't work")

            content_index = 3
        else:
            keys = await self.API.get_key_by_discord_id(message.author.id)
            if len(keys) > 1:
                head = 'You have multiple keys, please choose which one to update the info for by using `info set <key id> <information to set>`. Your keys are shown below'
                return await self.show_found_keys(keys, message.author, head=head)

            key = keys[0]
            content_index = 2

        split_content = message.content.split(maxsplit=content_index)
        try:
            info_text = split_content[content_index]
        except IndexError:
            return await message.author.send(usage)

        update = await self.NBE.set_info_for_key(key.id, info_text)
        if update:
            out = "Done!"
        else:
            out = "Sorry, that didn't work, please try again later"

        await message.author.send('Done!')

    async def show_found_keys(self, keys, mention, flavour=None, head=None):
        if len(keys) > 1:
            if not head:
                head = "You have been assigned the following keys"
            out = f"{head}: ```"
            for key in keys:
                out += f'{key.id} ({key.relative_priority} Priority)\n'
            out += f'```{flavour if flavour else ""}'
            await mention.send(out)
            return

        await mention.send(f"Your key is `{keys[0].id}`{' - ' if flavour else ''}{flavour}")
        return

    @staticmethod
    def key_with_priority(keys, priority):
        for key in keys:
            if key.priority == priority:
                return key

        return None

    async def key_find(self, user, quiet=False, priority=DEFAULT_PRIORITY):
        member = self.guild.get_member(user.id)
        if not member:
            return None, False

        keys = await self.API.get_key_by_discord_id(user.id)
        if keys is None:
            if not quiet:
                await user.send("Sorry, something went wrong. Please try again later")
            return None, False

        if keys and self.key_with_priority(keys, priority):
            await self.show_found_keys(keys, user, "have fun streaming!")
            return member, True

        name = member.nick if member.nick else member.name
        keys = await self.API.get_key_by_nick(name)
        if keys and self.key_with_priority(keys, priority):
            await self.show_found_keys(keys, user, "enjoy yourself!")
            return member, True

        return member, False

    async def key_generate_for_mention(self, mention, priority=DEFAULT_PRIORITY):
        member, key_found = await self.key_find(mention, quiet=True, priority=priority)
        if not member:
            return False

        name = member.nick if member.nick else member.name
        print(f'Generating key for {name}')
        if key_found:
            return True

        key = await self.API.new_key(name, member.id, priority)
        if key:
            await mention.send(f"Your key is `{key.id}` - bleep bloop!")

        return True

    async def on_ready(self):
        self.API = APIClient(KEYSERVER_URL)
        self.NBE = NightbotExtensions(NBE_URL, NBE_USER, NBE_PASSWORD)
        self.guild = self.get_guild(GUILD)
        self.notify_role = self.guild.get_role(NOTIFY_ROLE_ID)
        self.notification_channel = self.get_channel(NOTIFICATION_CHANNEL)
        print("Connected")

    async def stop(self):
        print("Stopping")
        await self.close()


if __name__ == '__main__':
    intents = discord.Intents.default()
    intents.members = True
    intents.reactions = True

    client = MyClient(intents=intents)

    loop = asyncio.get_event_loop()

    try:
        loop.run_until_complete(client.start(TOKEN))
    except KeyboardInterrupt:
        loop.run_until_complete(client.stop())
    finally:
        loop.close()
